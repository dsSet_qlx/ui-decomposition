import React, { useCallback, useReducer } from 'react';
import { size } from 'lodash';
import { KeyCode, useKeyManager } from './useKeyManager';
import { SearchFunction } from './types';

/**
 * We are using two separate flags to manage focus and open state of the suggest.
 * Cases:
 *  - Input on focus, but no search results -> opened = false
 *  - Input on focus and user click escape button -> opened = false
 *  - Input on focus and user click enter button to confirm suggest -> opened = false
 */
// eslint-disable-next-line no-shadow
enum Actions {
  // set loading state for the async operations.
  SetLoadingState = 'SetLoadingState',
  // Dispatch async operation errors.
  LoadError = 'LoadError',
  // Dispatch async operation success.
  ItemsLoaded = 'ItemsLoaded',
  // Action to manage focus state.
  SetFocus = 'SetFocus',
  // Action to manage open state.
  SetOpened = 'SetOpened',
  // Action to handle keyboard events.
  KeyPress = 'KeyPress',
}

interface AutoSuggestState<TItem> {
  items: Array<TItem>;
  loading: boolean;
  opened: boolean;
  focused: boolean;
}

interface AutoSuggestAction {
  type: Actions;
  payload?: unknown;
}

export interface AutoSuggestOptions<TItem> {
  onSearch: SearchFunction;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  onSuggest: (item: TItem) => void;
  limit: number;
}

const getDefaultState = <TItem>(): AutoSuggestState<TItem> => {
  return {
    items: [] as Array<TItem>,
    loading: false,
    opened: false,
    focused: false,
  };
};

const getDefaultOptions = <TItem>(): AutoSuggestOptions<TItem> => ({
  onSearch: () => Promise.resolve([]),
  onSuggest: () => {
    // noop
  },
  limit: 25,
});

const getOpenedState = <TItem>(
  state: AutoSuggestState<TItem>,
  nextOpenedState?: boolean,
): boolean => {
  const { items, focused } = state;
  // Opens suggest dropdown when input is in focus and we have items to display.
  return size(items) > 0 && focused ? Boolean(nextOpenedState) : false;
};

export const createReducer = <TItem>(): React.Reducer<
  AutoSuggestState<TItem>,
  AutoSuggestAction
> => {
  return (prevState, action) => {
    // Manage suggest open state
    if (action.type === Actions.SetOpened) {
      return {
        ...prevState,
        opened: getOpenedState(prevState, action.payload as boolean),
      };
    }

    // Manage input and dropdown focus state
    if (action.type === Actions.SetFocus) {
      const focused = Boolean(action.payload);
      return {
        ...prevState,
        focused,
        opened: focused
          ? getOpenedState({ ...prevState, focused }, true)
          : false,
      };
    }

    // Manage data fetching state
    if (action.type === Actions.SetLoadingState) {
      return {
        ...prevState,
        loading: Boolean(action.payload),
      };
    }

    // Put suggest fetch result to the state
    if (action.type === Actions.ItemsLoaded) {
      const items = action.payload as Array<TItem>;
      return {
        ...prevState,
        loading: false,
        items,
        opened: getOpenedState({ ...prevState, items }, true),
      };
    }

    // Handle fetch errors
    if (action.type === Actions.LoadError) {
      return {
        ...prevState,
        loading: false,
        items: [],
        opened: false,
      };
    }

    // Handle Escape and Enter keys to close dropdown
    if (
      action.type === Actions.KeyPress &&
      (action.payload === KeyCode.Escape || action.payload === KeyCode.Enter)
    ) {
      return {
        ...prevState,
        opened: false,
      };
    }

    // Handle Arrow keys to open dropdown
    if (
      action.type === Actions.KeyPress &&
      (action.payload === KeyCode.ArrowDown ||
        action.payload === KeyCode.ArrowUp) &&
      !prevState.opened
    ) {
      return {
        ...prevState,
        opened: getOpenedState(prevState, true),
      };
    }

    return prevState;
  };
};

/**
 * Hook data. Hook returns all data from state and handlers to manage it.
 */
interface AutoSuggestData<TItem> extends AutoSuggestState<TItem> {
  // Item selected with keyboard
  activeItem: number;
  // Keydown event handler
  handleKeypress: (e: React.KeyboardEvent) => void;
  // Handle select item to suggest
  handleSelect: (item: TItem) => void;
  // Handle focus events
  handleFocus: (state: boolean) => void;
  // Handle search event
  handleSearch: (value: string) => void;
}

/**
 * Auto Suggest data storage.
 */
export const useAutoSuggest = <TItem>(
  options: Partial<AutoSuggestOptions<TItem>> = {},
): AutoSuggestData<TItem> => {
  const suggestOptions = { ...getDefaultOptions<TItem>(), ...options };

  const [state, dispatch] = useReducer(
    createReducer<TItem>(),
    getDefaultState<TItem>(),
  );

  const handleSearch = useCallback(
    async (value: string) => {
      try {
        dispatch({ type: Actions.SetLoadingState, payload: true });
        const data = await suggestOptions.onSearch(value, suggestOptions.limit);
        dispatch({ type: Actions.ItemsLoaded, payload: data });
      } catch {
        dispatch({ type: Actions.LoadError });
      }
    },
    [suggestOptions.onSearch, suggestOptions.limit],
  );

  const handleFocus = useCallback((focused: boolean) => {
    dispatch({ type: Actions.SetFocus, payload: focused });
  }, []);

  const handleKeyPress = useCallback(
    (code: KeyCode, item?: TItem) => {
      dispatch({ type: Actions.KeyPress, payload: code });
      // call `onSuggest` callback if Enter was pressed and we have selected item to suggest
      if (code === KeyCode.Enter && suggestOptions.onSuggest && item) {
        suggestOptions.onSuggest(item);
      }
    },
    [suggestOptions.onSuggest],
  );

  const handleSelect = useCallback(
    (item: TItem) => {
      if (suggestOptions.onSuggest) {
        // call `onSuggest` callback on select action (for example user click on suggest item).
        suggestOptions.onSuggest(item);
      }

      dispatch({ type: Actions.SetOpened, payload: false });
    },
    [suggestOptions.onSuggest],
  );

  // Uses key manager to handle keyboard items selection
  const [activeIndex, onKeyPress] = useKeyManager<TItem>(
    state.items,
    handleKeyPress,
  );

  return {
    ...state,
    activeItem: activeIndex,
    handleKeypress: onKeyPress,
    handleSelect,
    handleFocus,
    handleSearch,
  };
};
