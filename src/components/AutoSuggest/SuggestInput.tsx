import React from 'react';
import styles from './styles.module.scss';

export const SuggestInput = React.forwardRef<
  HTMLInputElement,
  React.InputHTMLAttributes<HTMLInputElement>
>(({ children, ...props }, ref) => {
  return (
    <div className={styles.SearchControl}>
      <input className={styles.SearchControl__Input} ref={ref} {...props} />
      <div className={styles.SearchControl__Icon}>{children}</div>
    </div>
  );
});
