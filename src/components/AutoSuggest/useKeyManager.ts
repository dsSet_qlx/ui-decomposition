import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { size } from 'lodash';

// eslint-disable-next-line no-shadow
export enum KeyCode {
  Escape = 'Escape',
  Enter = 'Enter',
  ArrowUp = 'ArrowUp',
  ArrowDown = 'ArrowDown',
  Home = 'Home',
  End = 'End',
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const getFirstIndex = (collection: Array<unknown>): number => {
  return 0;
};

const getLastIndex = (collection: Array<unknown>): number => {
  return size(collection) - 1;
};

const getNextIndex = (collection: Array<unknown>, index: number): number => {
  return index === getLastIndex(collection)
    ? getFirstIndex(collection)
    : index + 1;
};

const getPrevIndex = (collection: Array<unknown>, index: number): number => {
  return index === getFirstIndex(collection)
    ? getLastIndex(collection)
    : index - 1;
};

/**
 * Hook to select item from the given collection. all keyboard events will be also handled with `handleKeyPress` callback.
 */
export const useKeyManager = <TItem>(
  collection: Array<TItem>,
  handleKeyPress?: (code: KeyCode, item?: TItem) => void,
): [number, (event: React.KeyboardEvent) => void] => {
  const [position, setPosition] = useState<number>(-1);

  // Clean up selection if collection was changed
  useEffect(() => {
    setPosition(-1);
  }, [collection]);

  // Handle keypress actions and set item selection
  const onKeypress = useCallback(
    (event: React.KeyboardEvent) => {
      const code = event.code as KeyCode;
      switch (code) {
        case KeyCode.ArrowDown:
          setPosition((index) => getNextIndex(collection, index));
          break;
        case KeyCode.ArrowUp:
          setPosition((index) => getPrevIndex(collection, index));
          break;
        case KeyCode.Home:
          setPosition(() => getFirstIndex(collection));
          break;
        case KeyCode.End:
          setPosition(() => getLastIndex(collection));
          break;
        default:
          break;
      }

      // call `handleKeyPress` callback if exist
      if (handleKeyPress) {
        handleKeyPress(code, collection[position]);
      }
    },
    [handleKeyPress, position, collection],
  );

  return useMemo(() => [position, onKeypress], [position, onKeypress]);
};
