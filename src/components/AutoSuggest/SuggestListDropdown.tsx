import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Modifier, usePopper } from 'react-popper';
import ReactDOM from 'react-dom';
import * as PopperJS from '@popperjs/core';

// PopperJS options, copied from the `usePopper` hook declaration
export type Options = Omit<Partial<PopperJS.Options>, 'modifiers'> & {
  createPopper?: typeof PopperJS.createPopper;
  modifiers?: ReadonlyArray<Modifier<unknown>>;
};

interface DropdownProps {
  children: [
    React.ReactElement<React.InputHTMLAttributes<HTMLInputElement>>,
    ...Array<React.ReactNode>
  ];

  // Dropdown options
  options?: Options;

  // Dropdown open state. This component is stateless!
  opened: boolean;

  // Callback to handle focus events
  onFocusChange: (state: boolean) => void;

  // Callback to handle keyboard events on dropdown
  onKeyDown?: (e: React.KeyboardEvent) => void;
}

/**
 * Dropdown component will listen the first element (needs to use HtmlInput) and display the second child as dropdown content on focus event
 *
 * Usage:
 *
 * ```ts
 *  const App = () => (
 *    <SuggestListDropdown options={{ placement: 'auto-start' }}>
 *      <input />
 *      <div>Dropdown Content</div>
 *    </SuggestListDropdown>
 *  );
 * ```
 *
 */
export const SuggestListDropdown: React.FC<DropdownProps> = ({
  opened,
  onFocusChange,
  onKeyDown,
  children,
  options,
}) => {
  // Dropdown content
  const [target, ...content] = children;

  const focusTimer = useRef<number>();

  // We are storing refs with useState hook cause following the react-popper documentation.
  // See popper example here https://popper.js.org/react-popper/v2/react-portals/
  const [targetRef, setTargetRef] = useState<HTMLElement | null>(null);
  const [contentRef, setContentRef] = useState<HTMLElement | null>(null);

  useEffect(() => {
    return () => window.clearInterval(focusTimer.current);
  }, []);

  // Popper JS
  const { styles, attributes } = usePopper(
    targetRef,
    contentRef,
    options || { placement: 'auto-start', strategy: 'fixed' },
  );

  // Focus Monitor
  // Add focus handler and passthrough original focus event
  const handleFocus = useCallback(
    (event: React.FocusEvent<HTMLInputElement>) => {
      if (target?.props?.onFocus) {
        target?.props?.onFocus(event);
      }
      window.clearInterval(focusTimer.current);
      if (!opened) {
        onFocusChange(true);
      }
    },
    [target.props.onFocus],
  );

  // Add blue handler and passthrough original blur event
  const handleBlur = useCallback(
    (event: React.FocusEvent<HTMLInputElement>) => {
      if (target?.props?.onBlur) {
        target?.props?.onBlur(event);
      }
      focusTimer.current = window.setTimeout(onFocusChange, 10, false);
    },
    [target?.props?.onBlur],
  );

  return (
    <>
      {/* Render first child of the dropdown element and add focus handlers */}
      <div ref={setTargetRef}>
        {React.cloneElement(target, {
          onBlur: handleBlur,
          onFocus: handleFocus,
        })}
      </div>
      {/* Render the second child element of dropdown as dropdown content */}
      {opened &&
        ReactDOM.createPortal(
          <div
            style={styles.popper}
            {...attributes.popper}
            ref={setContentRef}
            tabIndex={0}
            onBlur={handleBlur}
            onFocus={handleFocus}
            onKeyDown={onKeyDown}
          >
            {content}
          </div>,
          document.querySelector('body') as HTMLBodyElement,
        )}
    </>
  );
};
