import React, { useCallback } from 'react';
import { SuggestListDropdown, Options } from './SuggestListDropdown';
import { WithDebounce } from '../hoc';
import { SuggestInput as SearchInputComponent } from './SuggestInput';
import { SuggestBox } from './SuggestBox';
import { SpinIcon } from './SpinIcon';
import { useAutoSuggest } from './useAutoSuggest';
import { AutoSuggestItemProps } from './types';

// Wrap search input with debounce HOC.
export const SearchInput = WithDebounce(SearchInputComponent);

/**
 * Auto Suggest Props
 */
interface AutoSuggestProps<TItem>
  extends React.InputHTMLAttributes<HTMLInputElement> {
  // Search function to fetch suggests
  onSearch: (search: string, limit: number) => Promise<Array<TItem>>;

  // Component to render suggest item
  ItemComponent: React.FunctionComponent<AutoSuggestItemProps>;

  // Callback for user select value from the suggested list
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  onSuggest: (item: any) => void;

  // function to produce react key from suggest item object
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getItemKey: (item: any) => string;

  // Max suggest items to fetch from backend
  limit?: number;

  // Debounce timeout for user input
  timeout?: number;

  // Options to setup suggest dropdown. Inherited from popper js
  dropdownOptions?: Options;
}

// eslint-disable-next-line func-names
export const AutoSuggest = React.forwardRef(function <TItem>(
  {
    onSearch,
    limit,
    ItemComponent,
    onSuggest,
    getItemKey,
    timeout,
    dropdownOptions,
    onKeyDown,
    ...props
  }: AutoSuggestProps<TItem>,
  ref: React.ForwardedRef<HTMLInputElement>,
) {
  const {
    items,
    opened,
    loading,
    activeItem,
    handleFocus,
    handleSearch,
    handleKeypress,
    handleSelect,
  } = useAutoSuggest({
    onSuggest,
    onSearch,
    limit,
  });

  /**
   * Wrap up original keydown event with custom handler
   */
  const handleKeydownEvent = useCallback(
    (event: React.KeyboardEvent<HTMLInputElement>) => {
      if (onKeyDown) {
        onKeyDown(event);
      }

      handleKeypress(event);
    },
    [onKeyDown, handleKeypress],
  );

  return (
    <SuggestListDropdown
      opened={opened}
      onFocusChange={handleFocus}
      options={dropdownOptions}
      onKeyDown={handleKeypress}
    >
      <SearchInput
        {...props}
        ref={ref}
        timeout={timeout}
        onTimeout={handleSearch}
        onKeyDown={handleKeydownEvent}
      >
        {loading && <SpinIcon />}
      </SearchInput>
      <SuggestBox onKeyDown={handleKeydownEvent}>
        {items.map((item, index) => (
          <ItemComponent
            item={item}
            isActive={activeItem === index}
            key={getItemKey(item)}
            onItemSelect={handleSelect}
          />
        ))}
      </SuggestBox>
    </SuggestListDropdown>
  );
});
