import React from 'react';

import styles from './styles.module.scss';

export const SpinIcon: React.FC = () => {
  return (
    <svg
      className={styles.SpinIcon}
      width="32px"
      height="32px"
      viewBox="0 0 700 400"
      xmlns="http://www.w3.org/2000/svg"
      version="1.1"
    >
      <polygon
        fill="currentColor"
        stroke="currentColor"
        strokeWidth="10"
        points="350,75  379,161 469,161 397,215
                    423,301 350,250 277,301 303,215
                    231,161 321,161"
      />
    </svg>
  );
};
