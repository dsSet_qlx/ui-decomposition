/**
 * Function to fetch suggested list
 */
export type SearchFunction = (
  search: string,
  limit: number,
) => Promise<Array<unknown>>;

/**
 * Suggest item view properties
 */
export interface AutoSuggestItemProps {
  // Item to display
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  item: any;

  // Callback for user select
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  onItemSelect?: (item: any) => void;

  // Flag for selected item (for keyboard only)
  isActive: boolean;
}
