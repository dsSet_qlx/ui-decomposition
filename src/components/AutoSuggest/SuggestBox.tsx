import React from 'react';
import classNames from 'classnames';
import styles from './styles.module.scss';

export const SuggestBox: React.FC<React.HTMLAttributes<HTMLDivElement>> = ({
  children,
  className,
  ...props
}) => {
  return (
    <div {...props} className={classNames(className, styles.SuggestBox)}>
      {children}
    </div>
  );
};
