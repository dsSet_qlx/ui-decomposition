import React from 'react';

interface DebugProps {
  children: unknown;
}

export const Debug = ({ children }: DebugProps): React.ReactElement => {
  return (
    <pre className="App-debug">
      <h5>Debug:</h5>
      <hr />
      {JSON.stringify(children, null, 2)}
    </pre>
  );
};
