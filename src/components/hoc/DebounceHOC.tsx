import React, { ChangeEvent, useCallback, useEffect, useRef } from 'react';

const DEFAULT_TIMEOUT = 500;

interface DebounceProps {
  timeout?: number;
  onTimeout?: (value: string) => void;
}

/**
 * HOC to add debounce functionality to the input
 * @param Component
 * @constructor
 */
export const WithDebounce = <
  Props extends React.InputHTMLAttributes<HTMLInputElement>,
>(
  Component: React.FC<Props>,
): React.ForwardRefExoticComponent<
  React.PropsWithoutRef<
    React.InputHTMLAttributes<HTMLInputElement> & DebounceProps
  > &
    React.RefAttributes<HTMLInputElement>
> => {
  // Needs to use forward ref here to allow other code iterate with original dom input.
  return React.forwardRef<
    HTMLInputElement,
    React.InputHTMLAttributes<HTMLInputElement> & DebounceProps
  >(({ onTimeout, timeout, onChange, ...props }, ref) => {
    const timer = useRef<number>();
    // Wraps input onChange event with custom handler and passthrough original event
    const handleChange = useCallback(
      (event: ChangeEvent<HTMLInputElement>) => {
        // Fire original `onChange` event
        if (onChange) {
          onChange(event);
        }

        if (!onTimeout) {
          return;
        }

        // Clean up timer if exists
        if (timer.current) {
          window.clearTimeout(timer.current);
        }

        // Call `onTimeout` action in `timeout` ms with `event.target.value` value
        timer.current = window.setTimeout(
          onTimeout,
          timeout || DEFAULT_TIMEOUT,
          event.target.value,
        );
      },
      [timeout, onChange, onTimeout],
    );

    // Don't forget remove active timer on unmount!
    useEffect(() => {
      return () => {
        if (timer.current) {
          window.clearTimeout(timer.current);
        }
      };
    }, []);

    return (
      <Component {...(props as Props)} ref={ref} onChange={handleChange} />
    );
  });
};
