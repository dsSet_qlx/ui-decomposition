import React, { useCallback, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { User } from '../../../server/models/User';
import { AutoSuggestItemProps } from '../AutoSuggest';
import styles from '../AutoSuggest/styles.module.scss';

interface UserItemProps
  extends React.HTMLAttributes<HTMLDivElement>,
    AutoSuggestItemProps {
  item: User;
  isActive: boolean;
  onItemSelect?: (item: User) => void;
}

/**
 * Single item for the suggested list. We are refining item type here to work with User item instead of any.
 */
export const UserItem: React.FC<UserItemProps> = ({
  item,
  isActive,
  onItemSelect,
  className,
  ...props
}: UserItemProps) => {
  const elementRef = useRef<HTMLDivElement>(null);

  // Handle item active state to scroll parent container if needed.
  useEffect(() => {
    if (isActive && elementRef.current) {
      elementRef.current.scrollIntoView({
        behavior: 'auto',
        block: 'nearest',
      });
    }
  }, [isActive]);

  // Handle item click event
  const handleClick = useCallback(() => {
    if (onItemSelect) {
      onItemSelect(item);
    }
  }, [onItemSelect, item]);

  return (
    <div
      {...props}
      onClick={handleClick}
      ref={elementRef}
      className={classNames(className, styles.UserItem, {
        [styles['UserItem--Active']]: isActive,
      })}
    >
      <div>{item.EmailAddress}</div>
      <div className={styles.UserItem__Content}>
        <small>{item.FirstNameLastName}</small>
        <small>{item.JobTitle}</small>
      </div>
    </div>
  );
};
