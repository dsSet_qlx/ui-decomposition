import React, { FormEventHandler, useCallback, useRef, useState } from 'react';
import './App.css';
import { User } from '../server/models/User';
import { searchUsers } from './api';
import { AutoSuggest } from './components/AutoSuggest';
import { UserItem } from './components/SuggestItems';

const getItemKey = (i: User) => i.ID;

export const App: React.FC = () => {
  // Prevent form submit
  const handleKeypress = useCallback((event: React.KeyboardEvent) => {
    if (event.key === 'Enter') {
      event.preventDefault();
    }
  }, []);

  // Test controlled input
  const [inputValue, setInputValue] = useState('');
  const handleControlledSuggest = useCallback((item: User) => {
    setInputValue(item.EmailAddress);
  }, []);
  const handleControlledInputChanges = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setInputValue(event.target.value);
    },
    [],
  );

  // Test uncontrolled input
  const inputRef = useRef<HTMLInputElement>(null);
  const handleUncontrolledSuggest = useCallback((item: User) => {
    // setInputValue(item.EmailAddress);
    if (inputRef.current) {
      inputRef.current.value = item.EmailAddress;
    }
  }, []);
  const handleUncontrolledInputChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      // setInputValue(event.target.value);
      // eslint-disable-next-line no-console
      console.log('Change input value', event.target.value);
    },
    [],
  );
  return (
    <div className="App">
      <header className="App-header">
        <h1>UI Decomposition</h1>
      </header>
      <main className="App-content">
        <div className="App-field">
          <label>Controlled AutoSuggest</label>
          <AutoSuggest
            onSearch={searchUsers}
            ItemComponent={UserItem}
            getItemKey={getItemKey}
            onSuggest={handleControlledSuggest}
            limit={50}
            value={inputValue}
            onChange={handleControlledInputChanges}
            onKeyPress={handleKeypress}
            type="email"
            required
          />
        </div>

        <div className="App-field">
          <label>Uncontrolled AutoSuggest</label>
          <AutoSuggest
            onSearch={searchUsers}
            ItemComponent={UserItem}
            getItemKey={getItemKey}
            onSuggest={handleUncontrolledSuggest}
            limit={50}
            ref={inputRef}
            onChange={handleUncontrolledInputChange}
            onKeyPress={handleKeypress}
            type="email"
            required
          />
        </div>
      </main>
    </div>
  );
};
