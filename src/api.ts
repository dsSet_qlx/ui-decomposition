import axios from 'axios';
import { User } from '../server/models/User';

const client = axios.create({
  baseURL: 'http://localhost:3001',
  headers: {
    'Content-Type': 'application/json',
  },
});

export const searchUsers = async (
  search: string,
  limit: number,
): Promise<Array<User>> => {
  const { data } = await client.get<Array<User>>('/user', {
    params: { search, limit },
  });
  return data;
};
