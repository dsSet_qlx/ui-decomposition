import { readFile } from 'fs/promises';

export const getData = async <TResult>(path: string): Promise<TResult> => {
  const content = await readFile(path);
  return JSON.parse(content.toString('utf-8')) as TResult;
};

export const sleep = (min: number, max: number): Promise<void> => {
  const sleepTime = min + Math.round(Math.random() * (max - min));
  return new Promise((resolve) => {
    setTimeout(resolve, sleepTime);
  });
};
