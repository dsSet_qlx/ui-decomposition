import { resolve } from 'path';
import { filter, take } from 'lodash';
import { User } from './models/User';
import { getData } from './helpers';

const sourcePath = resolve(__dirname, './data/user.json');

const data: Promise<Array<User>> = getData(sourcePath);

export const searchUsers = async (
  search: string,
  limit = 50,
): Promise<Array<User>> => {
  const users = await data;
  const query = search.toLocaleLowerCase();

  if (!search) {
    return take(users, limit);
  }

  return take(
    filter(
      users,
      ({ FirstNameLastName, EmailAddress, JobTitle }) =>
        FirstNameLastName.toLocaleLowerCase().includes(query) ||
        EmailAddress.toLocaleLowerCase().includes(query) ||
        JobTitle.toLocaleLowerCase().includes(query),
    ),
    limit,
  );
};
