import express, { RequestHandler } from 'express';
import cors from 'cors';
import { sleep } from './helpers';
import { searchUsers } from './user';

const app = express();

app.use(cors());

const handleSearchUsers: RequestHandler = async (request, response) => {
  const { search, limit } = request.query;
  const searchParam = search ? String(search) : '';
  const limitNumber = Number.parseInt(String(limit), 10);
  const limitParam = Number.isFinite(limitNumber) ? limitNumber : 50;

  await sleep(100, 1000);
  const users = await searchUsers(searchParam, limitParam);
  response.send(users);
};

app.get('/user', handleSearchUsers);

app.listen(3001, () => {
  // eslint-disable-next-line no-console
  console.log(`App listening at http://localhost:${3001}`);
});
